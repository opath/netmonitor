import subprocess
import time

def getTXRXUtilization():

    p = subprocess.Popen("/sbin/ifconfig eth2", stdout=subprocess.PIPE, shell=True)
    (output, err) = p.communicate()
    outputString = ''.join(output)
    outputString = outputString.replace("bytes:", " ")

    res = [int(s) for s in outputString.split() if s.isdigit()]
    return res
old = getTXRXUtilization()
util = [0,0]
while (True):
    curr = getTXRXUtilization()
    util[0] = curr[0] - old[0]
    util[1] = curr[1] - old[1]
    old = curr

    time.sleep(1)
    if (util[0] > 10000000): 
        print util 

